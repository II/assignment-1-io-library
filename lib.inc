section .text

SYSCALL_EXIT equ 60
SYSCALL_READ equ 0
SYSCALL_WRITE equ 1
STDIN equ 0
STDOUT equ 1


; Accepts a return code and terminates the current process
exit:
    mov rax, SYSCALL_EXIT
    syscall

; Accepts a pointer to a null-terminated string and returns its length
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Accepts a pointer to a null-terminated string and prints it to stdout
print_string:
    push rdi
    call string_length ; rax = length of string

    pop rsi
    mov rdi, STDOUT
    mov rdx, rax ; rdx = length of string
    mov rax, SYSCALL_WRITE

    syscall
    ret

; Prints a character with code 0xA.
print_newline:
    mov rdi, `\n` ; ASCII code for newline
    ; Accepts a character code directly as its first argument and prints it to stdout.
    print_char:
        push rdi ; Save rdi

        mov rsi, rsp ; rsi = address of character
        mov rdi, STDOUT
        mov rdx, 1 ; length of character
        mov rax, SYSCALL_WRITE
        syscall
        
        pop rdi ; Restore rdi
        ret
; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi ; Set flags
    jns print_uint ; Jump if non-negative
        push rdi ; Save rdi
        mov rdi, '-'
        call print_char ; Print minus sign
        pop rdi ; Restore rdi
        neg rdi

    ; Выводит беззнаковое 8-байтовое число в десятичном формате 
    ; Совет: выделите место в стеке и храните там результаты деления
    ; Не забудьте перевести цифры в их ASCII коды.
    print_uint:
        ; Store current stack pointer
        mov rsi, rsp ; rsi = address of stack

        ; Make space for null-terminator
        dec rsp
        mov byte [rsp], 0 ; Null-terminate string

        ; Copy the number to rax
        mov rax, rdi

        ; Loop to extract each digit
        .extract_digit:
            ; Set divisor and clear remainder
            mov rdi, 10
            xor rdx, rdx ; rdx = 0

            ; Perform the division
            div rdi ; rax = rax / rdi, rdx = rax % rdi

            ; Convert remainder to ASCII and store on stack
            add dl, '0' ; Convert to ASCII
            dec rsp ; Make space for digit
            mov [rsp], dl ; Store remainder on stack

            ; If quotient is non-zero, continue
            test rax, rax ; Set flags
            jne .extract_digit ; Jump if not zero

        ; Print string from stack
        mov rdi, rsp ; rdi = address of string
        push rsi ; Save rsi
        call print_string
        pop rsi ; Restore rsi

        ; Reset the stack pointer
        mov rsp, rsi

        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.compare_loop:
    mov al, [rdi] ; Load byte from first string
    mov ah, [rsi] ; Load byte from second string

    cmp al, ah ; Compare bytes
    jne .not_equal ; If not equal, jump to .not_equal

    test al, al ; Set flags
    jz .equal ; If al is zero, jump to .equal

    inc rdi ; Load next byte from first string
    inc rsi ; Load next byte from second string

    jmp .compare_loop ; Continue comparing

.not_equal:
    xor rax, rax ; Set rax to 0 (strings are not equal)
    ret

.equal:
    mov rax, 1 ; Set rax to 1 (strings are equal)
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp ; Make space for character

    ; Read one character from stdin
    mov rax, SYSCALL_READ          
    mov rdi, STDIN
    mov rsi, rsp ; rsi = address of character
    mov rdx, 1 ; length of character
    syscall

    ; Check if EOF
    test rax, rax
    jz .eof

    movzx rax, byte [rsp] ; rax = character

.eof:
    ; Return 0
    inc rsp ; Reset the stack pointer
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12         ; Preserving callee-save registers
    push r13
    push r14

    mov r12, rdi     ; r12 to hold the address of the buffer
    mov r13, rsi     ; r13 to hold the size of the buffer
    dec r13          ; Decrease r13 by one for correct indexing
    xor r14, r14     ; r14 to hold the length of the word
    
    .loop:
        call read_char ; Call read_char to read a character from stdin
        test rax, rax ; Check if character is EOF
        je .success   ; Jump to .success if EOF reached and word length is not zero
        
        cmp rax, ' '  ; Check if character is a space
        je .check_success ; If yes, jump to .check_success
        cmp rax, `\t` ; Check if character is a tab
        je .check_success ; If yes, jump to .check_success
        cmp rax, `\n` ; Check if character is a newline
        je .check_success ; If yes, jump to .check_success
        
        cmp r14, r13  ; Compare word length to buffer size
        jge .error    ; If word is too long, jump to .error
        
        mov [r12+r14], rax ; Store the character in the buffer
        inc r14       ; Increment the word length
        jmp .loop     ; Continue with the next character
        
    .check_success:
        test r14, r14 ; Check if word length is zero
        jz .loop      ; If yes, continue with the next character
        
    .success:
        mov byte[r12+r14], 0 ; Null-terminate the string
        jmp .end      ; Jump to .end
        
    .error:
        xor r12, r12  ; Clear r12 to return 0
        
    .end:
        mov rax, r12  ; Load the buffer address into rax
        mov rdx, r14  ; Load the word length into rdx
        
    pop r14         ; Restoring callee-save registers
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax          ; Clear rax (result)
    xor rdx, rdx          ; Clear rdx (length of number)
.loop:
    ; Load character from string into cl
    movzx rcx, byte [rdi + rdx]

    ; Check if it is a digit
    test cl, cl
    jz .end               ; Jump to the end of string
    sub cl, '0'           ; Convert ASCII to integer
    cmp cl, 9             ; Check if it is a valid digit
    ja .end               ; Jump to failed if not a digit

    ; Add digit to result
    imul rax, rax, 10     ; Multiply result by 10
    add rax, rcx          ; Add digit to result

    inc rdx               ; Increment the length of number
    jmp .loop             ; Continue loop

.end:
    ret                   ; Return

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    ; Load the first character from the string into rcx
    movzx rcx, byte [rdi]
    
    ; Check for a leading minus sign
    cmp rcx, '-'
    jne parse_uint

    ; Skip the minus sign and parse the remaining string as a uint
    inc rdi
    call parse_uint
    
    ; Check if a number was successfully parsed
    test rdx, rdx
    jz .ParsingFailed
    
    ; Account for the length of the minus sign and negate the result
    inc rdx
    neg rax

.ParsingFailed:
    ret

.ParseAsPositive:
    ; Parse the string as a uint
    jmp parse_uint


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx ; Clear rcx (index)

.copy_loop:
    ; Load a byte from the source string into al
    movzx rax, byte[rdi + rcx]

    ; Check if the string will fit into the buffer
    cmp rcx, rdx
    jae .buffer_overflow

    ; Copy the byte to the destination buffer
    mov [rsi + rcx], al

    ; Check if we have reached the null terminator
    test al, al
    jz .null_terminator_reached

    ; Increment the index and loop
    inc rcx
    jmp .copy_loop

.null_terminator_reached:
    mov rax, rcx
    ret

.buffer_overflow:
    ; The string does not fit, return 0
    xor rax, rax
    ret
